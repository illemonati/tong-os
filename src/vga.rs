use core::fmt;
use spin::Mutex;
use lazy_static::lazy_static;

pub const VGA_TEXT_BUFFER_HEIGHT: usize = 25;
pub const VGA_TEXT_BUFFER_WIDTH: usize = 80;
pub const VGA_TEXT_BUFFER_ADDR: *mut VGATextBuffer = 0xb8000 as *mut VGATextBuffer;
pub const VGA_TEXT_LOWER_BOUND: u8 = 0x20;
pub const VGA_TEXT_UPPER_BOUND: u8 = 0x7e;
pub const VGA_TEXT_REPLACEMENT_CHAR: u8 = 0xfe;

lazy_static! {
    pub static ref DEFAULT_VGA_WRITER: Mutex<VGAWriter> = Mutex::new(VGAWriter::new_default());
}


macro_rules! writevga {
    ( $( $x:expr ), *) => {
        write!(VGAWriter::get_default().lock(), $($x,)*).unwrap()
    }
}
pub(crate) use writevga;



macro_rules! writevgaln {
    ( $( $x:expr ), *) => {
        writeln!(VGAWriter::get_default().lock(), $($x,)*).unwrap()
    }
}

pub(crate) use writevgaln;


#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum VGAColorU4 {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct VGAFullColor(u8);

impl VGAFullColor {
    pub fn new(foreground: VGAColorU4, background: VGAColorU4) -> VGAFullColor {
        VGAFullColor((background as u8) << 4 | (foreground as u8))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
struct VGAChar {
    char_code: u8,
    color: VGAFullColor
}

#[repr(transparent)]
pub struct VGATextBuffer {
    chars: [[VGAChar; VGA_TEXT_BUFFER_WIDTH]; VGA_TEXT_BUFFER_HEIGHT],
}


pub struct VGAWriter {
    pub col: usize,
    pub row: usize,
    pub buf: &'static mut VGATextBuffer,
    pub current_color: VGAFullColor
}

impl fmt::Write for VGAWriter {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_str(s);
        Ok(())
    }
}

// #[doc(hidden)]
// pub fn write_no_interrupt(args: fmt::Arguments) {
//     run_without_interrupt(|| {     // new
//         VGAWriter::get_default().lock().write_fmt(args).unwrap();
//     });
// }

impl VGAWriter {

    pub fn new_default() -> VGAWriter {
        VGAWriter {
            col: 0,
            row: 0,
            buf:  unsafe { &mut *VGA_TEXT_BUFFER_ADDR },
            current_color: VGAFullColor::new(VGAColorU4::Green, VGAColorU4::Black),
        }
    }

    pub fn get_default() -> &'static Mutex<VGAWriter> {
        &DEFAULT_VGA_WRITER
    }

    pub fn set_default_color(color: &VGAFullColor) {
        Self::get_default().lock().current_color = *color;
    }

    pub fn clear_default_screen() {
        Self::get_default().lock().clear_screen();
    }

    pub fn clear_screen(&mut self) {
        for row in 0..VGA_TEXT_BUFFER_HEIGHT {
            for col in 0..VGA_TEXT_BUFFER_WIDTH {
                self.buf.chars[row][col] = VGAChar {
                    char_code: b' ',
                    color: VGAFullColor::new(VGAColorU4::Black, VGAColorU4::Black)
                };
            }
        }
        self.row = 0;
        self.col = 0;
    }


    pub fn shift_up(&mut self) {
        for row in 1..VGA_TEXT_BUFFER_HEIGHT {
            for col in 0..VGA_TEXT_BUFFER_WIDTH {
                self.buf.chars[row-1][col] = self.buf.chars[row][col];

                if row == VGA_TEXT_BUFFER_HEIGHT-1 {
                    self.buf.chars[row][col] = VGAChar {
                        char_code: b' ',
                        color: VGAFullColor::new(VGAColorU4::Black, VGAColorU4::Black)
                    };
                }
            }
        }
        self.row -=1;
    }

    pub fn new_line(&mut self) {
        self.col = 0;
        if self.row >= VGA_TEXT_BUFFER_HEIGHT - 1 {
            self.shift_up();
        }
        self.row += 1;
    }

    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            b => {
                if self.col >= VGA_TEXT_BUFFER_WIDTH {
                    self.new_line();
                }
                self.buf.chars[self.row][self.col] = VGAChar {
                    char_code: b,
                    color: self.current_color,
                };
                self.col += 1;
            }
        }
    }

    pub fn write_str(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                VGA_TEXT_LOWER_BOUND..=VGA_TEXT_UPPER_BOUND | b'\n' => self.write_byte(byte),
                _ => self.write_byte(VGA_TEXT_REPLACEMENT_CHAR)
            }
        }
    }

}