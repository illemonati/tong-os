use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use crate::vga::{writevgaln};
use crate::vga::{VGAWriter};
use core::fmt::Write;
use lazy_static::lazy_static;
use pic8259::ChainedPics;
use spin::Mutex;
use crate::configs::KEYBOARD_DATA_PORT;
use x86_64::instructions::port::Port;

use crate::input_utils::handle_keyboard_input;


// PIC maps to 0 - 8
// but 0-32 is used for CPU and faults
pub const PIC_1_OFFSET: u8 = 32;
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

pub static CHAINED_PROGRAMMABLE_INTERRUPT_CONTROLLER: Mutex<ChainedPics> = Mutex::new(unsafe {
    ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET)
});

lazy_static! {
    static ref INTERRUPT_DESCRIPTOR_TABLE: InterruptDescriptorTable = create_idt();
}

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum HardwareInterruptIndex {
    Timer = PIC_1_OFFSET,
    Keyboard = PIC_1_OFFSET + 1,
}

pub fn init_pic() {
    unsafe {
        CHAINED_PROGRAMMABLE_INTERRUPT_CONTROLLER.lock().initialize();
    }
}

pub fn create_idt() -> InterruptDescriptorTable {
    let mut table = InterruptDescriptorTable::new();
    table.breakpoint.set_handler_fn(breakpoint_interrupt_handler);
    table.double_fault.set_handler_fn(double_fault_interrupt_handler);
    table[HardwareInterruptIndex::Timer as usize].set_handler_fn(timer_interrupt_handler);
    table[HardwareInterruptIndex::Keyboard as usize].set_handler_fn(keyboard_interrupt_handler);
    table
}

pub fn init_idt() {
        INTERRUPT_DESCRIPTOR_TABLE.load();
}

pub fn notify_end_of_interrupt(interrupt_index: HardwareInterruptIndex) {
    unsafe {
        CHAINED_PROGRAMMABLE_INTERRUPT_CONTROLLER.lock().notify_end_of_interrupt(interrupt_index as u8);
    }
}

extern "x86-interrupt" fn breakpoint_interrupt_handler(stack_frame: InterruptStackFrame) {
    writevgaln!("Breakpoint Interrupt:{:#?}", stack_frame);
}

extern "x86-interrupt" fn double_fault_interrupt_handler(stack_frame: InterruptStackFrame, _error_code: u64) -> ! {
    panic!("EXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn timer_interrupt_handler(_stack_frame: InterruptStackFrame) {
    // writevga!("timer");
    notify_end_of_interrupt(HardwareInterruptIndex::Timer);
}

extern "x86-interrupt" fn keyboard_interrupt_handler(_stack_frame: InterruptStackFrame) {

    let mut port: Port<u8> = Port::new(KEYBOARD_DATA_PORT);
    let key_scancode = unsafe {
        port.read()
    };

    handle_keyboard_input(key_scancode);


    notify_end_of_interrupt(HardwareInterruptIndex::Keyboard);
}