use crate::input_utils::wait_for_any_key;
use crate::vga::{VGAWriter, writevga};
use core::fmt::Write;

pub fn start_notepad() {
    VGAWriter::clear_default_screen();
    loop {
        let character = wait_for_any_key();
        match character as i32 {
            0x1b => {
                break;
            },
            _ => {
                writevga!("{}", character);
            }
        }
    }
    VGAWriter::clear_default_screen();
}