use core::arch::asm;


#[allow(dead_code)]
pub fn enable_interrupts() {
    unsafe {asm!("sti", options(nomem, nostack));}
}

#[allow(dead_code)]
pub fn disable_interrupts() {
    unsafe {asm!("cli", options(nomem, nostack));}
}

#[allow(dead_code)]
pub fn run_without_interrupt<F, R>(f: F) -> R
    where F: FnOnce() -> R, {
    disable_interrupts();
    let r = f();
    enable_interrupts();
    r
}
pub fn halt_until_next_interrupt() {
    unsafe {
        asm!("hlt", options(nomem, nostack));
    }
}

pub fn halt_forever() -> !{
    loop {
        halt_until_next_interrupt();
    }
}