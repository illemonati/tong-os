use core::panic::PanicInfo;
use crate::VGAWriter;
use core::fmt::Write;
use crate::asm_utils::halt_forever;
use crate::vga::writevgaln;

#[panic_handler]
fn panic(panic_info: &PanicInfo) -> ! {
    writevgaln!("Panicked: {:#?}", panic_info.message().unwrap());
    halt_forever()
}


