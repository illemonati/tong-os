use crate::vga::{VGAColorU4, VGAFullColor, writevga, writevgaln};
use crate::vga::VGAWriter;
use core::fmt::Write;
use core::str;
use crate::display_startup_message;
use crate::input_utils::wait_for_any_key;
use crate::notepad::start_notepad;

pub fn launch_shell_clear_screen() {
    launch_shell();
}

pub fn launch_shell() {
    VGAWriter::clear_default_screen();
    let mut command_buf: [u8; 500] = [0u8; 500];
    let mut command_buf_index: usize = 0;
    loop {
        writevga!("Tong Shell> ");
        loop {
            let character = wait_for_any_key();
            match character {
                '\n' => {
                    writevgaln!("");
                    let command_str = str::from_utf8(&command_buf[0..command_buf_index]).unwrap();
                    handle_command(command_str);
                    command_buf_index = 0;
                    break;
                },
                _ => {
                    if command_buf_index >= command_buf.len() {
                        continue;
                    }
                    writevga!("{}", character);
                    character.encode_utf8(&mut command_buf[command_buf_index..]);
                    command_buf_index += character.len_utf8();

                }
            }
        }
    }
}

fn handle_command(command_str: &str) {
    match command_str {
        "help" => {
            handle_help_command(command_str);
        },
        "notepad" => {
            handle_notepad_command(command_str);
        },
        "startup" => {
            handle_startup_screen(command_str);
        }
        _ => {
            handle_unknown_command();
        }
    }
    writevgaln!();
    VGAWriter::set_default_color(&VGAFullColor::new(VGAColorU4::Green, VGAColorU4::Black));
}


fn handle_unknown_command() {
    writevgaln!("Unknown Command, use <help> for list of commands");
}

fn handle_notepad_command(_command_str: &str) {
    start_notepad();
}

fn handle_startup_screen(_command_str: &str) {
    display_startup_message();
}

fn handle_help_command(_command_str: &str) {
    writevgaln!("Command Available:");
    writevgaln!("help -- print this prompt");
    writevgaln!("notepad -- open a notepad");
    writevgaln!("startup -- display startup screen");
}