#![feature(abi_x86_interrupt)]
#![feature(panic_info_message)]
#![no_std]
#![no_main]


use crate::vga::{VGAColorU4, VGAFullColor, VGAWriter};
use core::fmt::Write;
use crate::asm_utils::{enable_interrupts, halt_forever};
use crate::configs::{OS_GREETING, OS_LOGO, VERSION};
use crate::input_utils::wait_for_any_key;
use crate::interrupt::{init_idt, init_pic};
use crate::shell::launch_shell_clear_screen;
use crate::vga::writevgaln;

mod panic;
mod vga;
mod configs;
mod interrupt;
mod asm_utils;
mod input_utils;
mod shell;
mod notepad;


fn init() {
    init_idt();
    init_pic();
    // STI instruction to enable external interrupts
    enable_interrupts();

}

fn display_startup_message() {
    VGAWriter::clear_default_screen();
    VGAWriter::set_default_color(&VGAFullColor::new(VGAColorU4::Green, VGAColorU4::Yellow));
    writevgaln!("{}", OS_GREETING);
    VGAWriter::set_default_color(&VGAFullColor::new(VGAColorU4::Cyan, VGAColorU4::Black));
    writevgaln!("-----------------------------------------");
    writevgaln!("-----------------------------------------");
    writevgaln!("{}", OS_LOGO);
    writevgaln!("-----------------------------------------");
    writevgaln!("-----------------------------------------");
    VGAWriter::set_default_color(&VGAFullColor::new(VGAColorU4::Green, VGAColorU4::Black));
    writevgaln!("Version {}", VERSION);
    writevgaln!("Good Luck and Have Fun!");
    writevgaln!("Press any key to continue...");
    wait_for_any_key();
    VGAWriter::clear_default_screen();
}




#[no_mangle]
pub extern "C" fn _start() -> ! {
    init();
    display_startup_message();
    launch_shell_clear_screen();
    halt_forever()
}
