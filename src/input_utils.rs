use crate::asm_utils::halt_until_next_interrupt;
use lazy_static::lazy_static;
use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
use spin::Mutex;



lazy_static! {
    static ref KEYBOARD: Mutex<Keyboard<layouts::Us104Key, ScancodeSet1>> =
        Mutex::new(Keyboard::new(ScancodeSet1::new(), layouts::Us104Key, HandleControl::Ignore));
    static ref KEYS_INPUTTED: Mutex<KeysInputtedBuffer> = Mutex::new(KeysInputtedBuffer::new());
}


struct KeysInputtedBuffer {
    buffer: [char; 500],
    index: usize,
    currently_contained: usize,
}

impl KeysInputtedBuffer {
    pub fn new() -> Self {
        Self {
            buffer: [char::from(0); 500],
            index: 0,
            currently_contained: 0
        }
    }

    pub fn add(&mut self, character: char) {
        if self.index >= self.buffer.len() {
            self.index %= self.buffer.len();
        }
        self.buffer[self.index] = character;
        self.index += 1;
        self.currently_contained += 1;
    }

    pub fn get_one(&mut self) -> Option<char> {
        if self.currently_contained < 1 {
            return None;
        }
        if self.index < 1 {
            self.index = self.buffer.len();
        }
        self.index -= 1;
        self.currently_contained -= 1;
        Some(self.buffer[self.index])
    }


}


pub fn wait_for_any_key () -> char {
    loop {
        halt_until_next_interrupt();
        if let Some(character) = KEYS_INPUTTED.lock().get_one() {
            return character;
        }
    }
}



pub fn handle_keyboard_input(key_scancode: u8) {
    let mut keyboard = KEYBOARD.lock();
    if let Ok(Some(key_event)) = keyboard.add_byte(key_scancode) {
        if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(character) => {
                    KEYS_INPUTTED.lock().add(character);
                },
                DecodedKey::RawKey(_key) => {

                }
            }
        }
    }
}